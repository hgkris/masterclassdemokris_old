<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 11-5-14
 * Time: 21:14
 */

return array(

    'default_active' => true,

    'default_published' => false

);