@extends('layouts.bootstrap')

@section('title')
User settings
@stop

@section('content')

<h1>User Settings</h1>

<div class="jumbotron text-center">
    <h2>{{ Auth::user()->name }}</h2>
    <p>
        <strong>Email:</strong> {{  Auth::user()->email }}<br>
        <strong>Role:</strong> {{ Auth::user()->role }}
    </p>
</div>
@stop