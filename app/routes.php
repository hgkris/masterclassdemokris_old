<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

require_once "routes_cache.php";
require_once "routes_sessions.php";
require_once "routes_admin.php";

Route::resource('categories', 'CategoryController');
Route::resource('comments', 'CommentsController');
Route::resource('posts', 'PostsController');
Route::resource('tags', 'TagsController');

// Route::resource('pages', 'PagesController');

Route::model('post', 'Post');

Route::group(
    array('prefix' => 'v2'),
    function () {
        Route::get('/post', array('as' => 'posts', 'uses' => 'PostController@index'));
        Route::get('/post/create', array('as' => 'post.create', 'uses' => 'PostController@create'));
        Route::get('/post/{post}/edit', array('as' => 'post.edit', 'uses' => 'PostController@edit'));
        Route::get('/post/{post}', array('as' => 'post', 'uses' => 'PostController@show'));
        Route::get('/post/{post}/delete', array('as' => 'post.delete', 'uses' => 'PostController@delete'));

        Route::post('/post/create', array('as' => 'post.create', 'uses' => 'PostController@handleCreate'));
        Route::post('/post/{post}/edit', array('as' => 'post.edit', 'uses' => 'PostController@handleEdit'));
        Route::post('/post/{post}/delete', array('as' => 'post.delete', 'uses' => 'PostController@handleDelete'));
    }
);

Route::group(
    array('prefix' => 'v1'),
    function () {
        Route::get(
            '/post',
            function () {
                return View::make('post.index');
            }
        );

        Route::get(
            '/post/create',
            function () {
                return View::make('post.create');
            }
        );

        Route::get(
            '/post/{post}',
            function () {
                return View::make('post.show');
            }
        );

        Route::get(
            '/post/{post}/edit',
            function () {
                return View::make('post.edit');
            }
        );
    }
);

Route::group(
    array('prefix' => 'v0'),
    function () {
        Route::get(
            '/post',
            function () {
                return "Show the Posts";
            }
        );

        Route::get(
            '/post/create',
            function () {
                return "Create a Post";
            }
        );

        Route::get(
            '/post/{post}',
            function () {
                return "View a Post";
            }
        );
    }
);

Route::get(
    '/login',
    array(
        'as' => 'login',
        function () {
            return View::make('auth.login');
        }
    )
);

Route::post(
    '/login',
    array(
        'as' => 'login',
        function () {
            if (Auth::loginUsingId(1)) {
                return View::make('auth.login_success');
            } else {
                return View::make('auth.login_failed');
            }

        }
    )
);

Route::get(
    '/logout',
    array(
        'as' => 'logout',
        function () {
            Auth::logout();

            return View::make('auth.logout');
        }
    )
);

Route::group(
    array('before' => 'auth'),
    function () {
        Route::get(
            '/auth/settings',
            array(
                'as' => 'user.settings',
                function () {
                    return View::make('user.settings');
                }
            )
        );
    }
);


Route::get(
    'lang/{lang}',
    function ($lang) {
        $languages = Config::get('app.languages');

        if (in_array($lang, $languages)) {
            Session::put('lang', $lang);
        }

        return Redirect::back();
    }
);


Route::get(
    '/about',
    array(
        'as' => 'about',
        function () {
            return View::make('pages.about');
        }
    )
);


Route::get(
    '/contact',
    array(
        'as' => 'contact',
        function () {
            return View::make('pages.contact');
        }
    )
);

Route::get(
    '/',
    array(
        'as' => 'home',
        function () {
            return View::make('pages.home');
        }
    )
);

Route::get(
    '/show',
    function () {
        return View::make('hello');
    }
);

Route::get('/showme', 'HomeController@showWelcome');

//Route::get(
//    'pages',
//    function () {
//
//    }
//);

Route::get(
    'user/{name?}',
    function ($name = 'John') {
        return $name;
    }
);

//Route::get(
//    '{lang?}/{slug?}',
//    function ($lang = 'nl', $slug = 'default') {
////  dezesite.nl/nl/about
////  dezesite.nl/fr/apro
//        return "Get the $lang page with $slug";
//    }
//);

//App::missing(function($exception)
//    {
//        return Response::view('error', array(), 404);
//    });

Route::post('category', array('as' => 'category.create', 'uses' => 'CategoryController@store'));
