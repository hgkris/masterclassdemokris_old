<?php

use Carbon\Carbon;

class TagsController extends \BaseController {

	/**
	 * Display a listing of tags
	 *
	 * @return Response
	 */
	public function index()
	{
        $expiresAt = Carbon::now()->addMinutes(5);
        $list = Cache::remember('tags', $expiresAt, function()
        {
            return Tag::all();
        });

		return View::make('tags.index', compact('list'), array('resource' => 'tags'));
	}

	/**
	 * Show the form for creating a new tag
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('tags.create', array('resource' => 'tags'));
	}

	/**
	 * Store a newly created tag in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Tag::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Tag::create($data);

        Session::flash('message', 'Tag created');

		return Redirect::route('tags.index', array('resource' => 'tags'));
	}

	/**
	 * Display the specified tag.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$item = Tag::findOrFail($id);

		return View::make('tags.show', compact('item'), array('resource' => 'tags'));
	}

	/**
	 * Show the form for editing the specified tag.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $item = Tag::find($id);

		return View::make('tags.edit', compact('item'), array('resource' => 'tags'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $item = Tag::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Tag::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

        $item->update($data);

        Session::flash('message', 'Tag edited and saved');

		return Redirect::route('tags.index', array('resource' => 'tags'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Tag::destroy($id);

        Session::flash('message', 'Tag destroyed');

		return Redirect::route('tags.index', array('resource' => 'tags'));
	}

}